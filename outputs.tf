output "vpc-id" {
  value = data.aws_vpc.test.id
}

output "vpc-arn" {
  value = data.aws_vpc.test.arn
}
